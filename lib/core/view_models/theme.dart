import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../theme/theme_data.dart';
import '../../theme/theme_light.dart';

class ThemeViewModel extends ChangeNotifier {
  bool isDark = false;
  AppTheme appTheme = themeLight;

  void changeTheme() {
    if (isDark) {
      isDark = false;
      appTheme = themeLight;
    } else {
      isDark = true;
      // Todo for now I have dfine same light theme we can define here dark theme
      appTheme = themeLight;
    }

    notifyListeners();
  }
}

AppTheme appTheme(BuildContext context) {
  return Provider.of<ThemeViewModel>(context).appTheme;
}
