class TagsDataModel {
  String image;
  String title;

  TagsDataModel({
    required this.image,
    required this.title,
  });
}
