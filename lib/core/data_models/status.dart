class StatusDataModel {
  String image;
  String userImage;
  String name;

  StatusDataModel({
    required this.image,
    required this.name,
    required this.userImage,
  });
}
