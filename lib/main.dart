import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'core/view_models/theme.dart';
import 'view/views/dashboard_widgets/dashboard.dart';
import 'view_models/tags_vm.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ThemeViewModel()),
        ChangeNotifierProvider(create: (context) => TagsViewModel())
      ],
      child: Consumer<ThemeViewModel>(builder: (context, themeModelVm, _) {
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'facebook',
            theme: themeModelVm.appTheme.themeData,
            home: const Dashboard());
      }),
    );
  }
}
