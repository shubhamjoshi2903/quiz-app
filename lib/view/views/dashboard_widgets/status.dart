import 'package:flutter/material.dart';
import 'package:facebook_app/core/data_models/tags.dart';

Widget status(BuildContext context, {required TagsDataModel tags}) {
  // print(tags)
  return Container(
    width: 150,
    decoration: const BoxDecoration(
      color: Colors.white,
    ),
    child: Container(
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        image: const DecorationImage(
            image: AssetImage("assets/images/img5.jpg"), fit: BoxFit.fitHeight),
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: const Color.fromARGB(255, 213, 211, 211),
        ),
      ),
      child: Stack(
        children: [
          Positioned(
              top: 5,
              left: 10,
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 5, 127, 226),
                    borderRadius: BorderRadius.circular(50),
                    border: Border.all(
                      color: Colors.white,
                      width: 3,
                    )),
                child: const CircleAvatar(
                  backgroundImage: AssetImage('assets/images/myImages.jpeg'),
                  minRadius: 30,
                  maxRadius: 30,
                ),
              )),
          Positioned(
            top: 190,
            left: 8,
            child: Text(
              tags.title,
              style: const TextStyle(fontSize: 16, color: Colors.white),
            ),
          )
        ],
      ),
    ),
  );
}
