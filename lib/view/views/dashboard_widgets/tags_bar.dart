import 'package:flutter/material.dart';
import 'package:facebook_app/core/data_models/tags.dart';

Widget tagBar(BuildContext context, {required TagsDataModel tags}) {
  // print(tags)
  return Container(
    decoration: const BoxDecoration(color: Color.fromARGB(66, 198, 201, 207)),
    child: Container(
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: Color.fromARGB(255, 213, 211, 211),
        ),
      ),
      child: Row(
        
        children: [
          SizedBox(height: 40, width: 50, child: Image.asset(tags.image)),
          Text(
            tags.title,
            style: const TextStyle(fontSize: 20),
          ),
          const SizedBox(
            width: 20,
          )
        ],
      ),
    ),
  );
}
