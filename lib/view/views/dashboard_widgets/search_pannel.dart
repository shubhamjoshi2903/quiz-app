import 'package:flutter/material.dart';

Widget searchPannel({
  EdgeInsets margin = const EdgeInsets.only(left: 12),
}) {
  return Container(
    margin: const EdgeInsets.all(20),
    child: Row(
      children: const [
        CircleAvatar(
          backgroundImage: AssetImage('assets/images/myImages.jpeg'),
          minRadius: 30,
          maxRadius: 30,
        ),
        Expanded(
          child: TextField(
            decoration: InputDecoration(
              labelStyle: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.w500,
                  fontSize: 18),
              border: InputBorder.none,
              labelText: "What's on your mind?",
              contentPadding: EdgeInsets.only(left: 15),
            ),
          ),
        ),
        Icon(
          Icons.image_sharp,
          color: Colors.green,
          size: 30.0,
          textDirection: TextDirection.ltr,
          semanticLabel: 'Icon',
        ),
      ],
    ),
  );
}
