import 'package:flutter/material.dart';
import 'package:facebook_app/core/data_models/tags.dart';

Widget status_upload() {
  // print(tags)
  return Container(
    width: 150,
    decoration: const BoxDecoration(color: Colors.white),
    child: Container(
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
      ),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            child: Image.asset(
              "assets/images/myImages.jpeg",
              height: 128,
              width: 128,
            ),
          ),
          Positioned(
              top: 170,
              child: Container(
                margin: const EdgeInsets.only(left: 15),
                width: 100,
                child: const Center(
                  child: Text(
                    "Create a Story",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                ),
              )),
          Positioned(
              top: 100,
              left: 30,
              child: Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 5, 127, 226),
                    borderRadius: BorderRadius.circular(50),
                    border: Border.all(
                      color: Colors.white,
                      width: 5,
                    )),
                child: Container(
                  margin: const EdgeInsets.only(left: 17, top: 6),
                  child: const Text(
                    '+',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                    ),
                  ),
                ),
              ))
        ],
      ),
    ),
  );
}
