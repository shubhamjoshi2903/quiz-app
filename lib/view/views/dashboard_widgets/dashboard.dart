import 'package:flutter/material.dart';
import 'package:facebook_app/view/views/dashboard_widgets/search_pannel.dart';
import 'package:facebook_app/view/views/dashboard_widgets/status.dart';
import 'package:facebook_app/view/views/dashboard_widgets/tags_bar.dart';
import 'package:provider/provider.dart';
import 'package:facebook_app/view_models/tags_vm.dart';
import 'package:facebook_app/view/views/dashboard_widgets/upload_status.dart';


class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> {
TagsViewModel? tagsDataModel;
@override
  @override
  void initState() {
    tagsDataModel = context.read<TagsViewModel>();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: CustomScrollView(
            slivers: [
              SliverAppBar(
                pinned: true,
                backgroundColor: Colors.white,
                elevation: 0,
                flexibleSpace: Container(
                  margin: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text(
                            'facebook',
                            style: TextStyle(
                                fontSize: 30,
                                color: Color.fromARGB(255, 5, 127, 226),
                                fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 219, 215, 215),
                                borderRadius: BorderRadius.circular(20)),
                            height: 40,
                            child: const Icon(
                              Icons.search_rounded,
                              color: Colors.black,
                              size: 31.0,
                            ),
                          ),
                          const SizedBox(
                            width: 11,
                          ),
                          Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 219, 215, 215),
                                borderRadius: BorderRadius.circular(20)),
                            height: 40,
                            child: Image.asset('assets/images/messanger.png'),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: searchPannel(),
              ),
              const SliverToBoxAdapter(
                child: SizedBox(
                  height: 10,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  width: 500,
                  color: Color(0xcbcfd1),
                  height: 70,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: tagsDataModel!.tags.length,
                      itemBuilder: (context, index) {
                        return tagBar(context,
                            tags: tagsDataModel!.tags[index]);
                      }),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  color: Color.fromARGB(137, 137, 143, 156),
                  height: 15,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  width: 500,
                  color: Colors.white,
                  height: 250,
                  child: Row(
                    children: [
                      Expanded(
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: tagsDataModel!.tags.length,
                            itemBuilder: (context, index) {
                              return (index == 0
                                  ? status_upload()
                                  : status(context,
                                      tags: tagsDataModel!.tags[index]));
                            }),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  color: Color.fromARGB(137, 137, 143, 156),
                  height: 15,
                ),
              ),
            ],
          )),
    );
  }
}

