import 'package:flutter/material.dart';

// [ Here define color custom color name ]

class AppColors {
  Color primary;
  Color greyText;
  Color white;
  Color black;
  Color greyBackground;
  Color courseItemViewbackground1;
  Color courseItemViewbackground2;
  AppColors(
      {required this.primary,
      required this.greyText,
      required this.black,
      required this.greyBackground,
      required this.white,
      required this.courseItemViewbackground1,
      required this.courseItemViewbackground2});
}

// [ Theme model ]

class AppTheme {
  AppColors colors;
  ThemeData themeData;

  AppTheme({required this.colors, required this.themeData});
}
