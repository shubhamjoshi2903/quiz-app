import 'package:flutter/material.dart';
import 'theme_data.dart';

const _primaryColor = Color(0xff00b6f0);

AppTheme themeLight = AppTheme(
  colors: AppColors(
      primary: _primaryColor,
      greyText: const Color(0xff808180),
      black: const Color(0xff060608),
      white: Colors.white,
      greyBackground: const Color(0xfff4f9fc),
      courseItemViewbackground1: const Color(0xffd3d3d5),
      courseItemViewbackground2: const Color(0xfff9fbfb)),
  // [ this themeData ]
  themeData: ThemeData(
    primaryColor: _primaryColor,
    iconTheme: const IconThemeData(color: Colors.white),
    colorScheme: const ColorScheme.light().copyWith(
      primary: _primaryColor,
      secondary: _primaryColor,
    ),
  ),
);
